package cl.brank.brank.models;

/**
 * Created by Jorge on 21-04-2017.
 */

public class Contrincante {
    private String nombre;
    private String urlImagen;
    private int edad;
    private String categoria;
    private String ranking;
    private String horario;

    public Contrincante(String nombre, String urlImagen, int edad, String categoria, String ranking, String horario) {
        this.nombre = nombre;
        this.urlImagen = urlImagen;
        this.edad = edad;
        this.categoria = categoria;
        this.ranking = ranking;
        this.horario = horario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUrlImagen() {
        return urlImagen;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getRanking() {
        return ranking;
    }

    public void setRanking(String ranking) {
        this.ranking = ranking;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }
}
