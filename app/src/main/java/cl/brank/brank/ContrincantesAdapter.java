package cl.brank.brank;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import cl.brank.brank.models.Contrincante;
import cl.brank.brank.models.UserValues;

/**
 * Created by Jorge on 19-04-2017.
 */

public class ContrincantesAdapter extends ArrayAdapter<Contrincante> {
    private ArrayList<Contrincante> dataSource;

    public ContrincantesAdapter(Context context, ArrayList<Contrincante> dataSource) {
        super(context, R.layout.item_list_contrincante, dataSource);
        this.dataSource = dataSource;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View item = inflater.inflate(R.layout.item_list_contrincante, null);

        TextView tvHorario = (TextView) item.findViewById(R.id.tvHorario);
        tvHorario.setText("" + dataSource.get(position).getHorario());

        TextView tvCategoria = (TextView) item.findViewById(R.id.tvCategoria);
        tvCategoria.setText(dataSource.get(position).getCategoria());

        TextView tvNombre = (TextView) item.findViewById(R.id.tvNombre);
        tvNombre.setText(dataSource.get(position).getNombre());

        TextView tvEdad = (TextView)item.findViewById(R.id.tvEdad);
        tvEdad.setText("Edad: "+dataSource.get(position).getEdad()+" Años");

        TextView tvRanking = (TextView)item.findViewById(R.id.tvRanking);
        tvRanking.setText(dataSource.get(position).getRanking());

        ImageView ivPreview = (ImageView) item.findViewById(R.id.ivAvatar);
        Picasso.with(getContext())
                .load(dataSource.get(position).getUrlImagen())
                .into(ivPreview);


        return (item);
    }
}
