package cl.brank.brank;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cl.brank.brank.models.Categoria;
import cl.brank.brank.models.UserValues;
import cl.brank.brank.preferences.SesionPreferencesUser;
import cl.brank.brank.util.Util;

public class AjustesDeBusquedaActivity extends AppCompatActivity {

    private ImageView imgPerfil;
    private FirebaseAuth mAuth;
    private UserValues userValues;

    private TextView txtCategoria;
    private TextView txtNombre;

    private Button btnAgregarHorario;

    private TextView txtHorarioDisponible;

    private LinkedHashMap<String, String> horarioDisponible;

    private final String LUNES = "LUNES";
    private final String MARTES = "MARTES";
    private final String MIERCOLES = "MIÉRCOLES";
    private final String JUEVES = "JUEVES";
    private final String VIERNES = "VIERNES";
    private final String SABADO = "SÁBADO";
    private final String DOMINGO = "DOMINGO";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajustes_de_busqueda);



        mAuth = FirebaseAuth.getInstance();

        userValues = SesionPreferencesUser.getInstance().getUserSesion(this);

        horarioDisponible = new LinkedHashMap<>();
        txtCategoria = (TextView)findViewById(R.id.txtCategoria);
        txtCategoria.setText(userValues.getCategory());

        txtHorarioDisponible = (TextView)findViewById(R.id.txtHorarioDisponible);

        txtNombre = (TextView)findViewById(R.id.txtNombre);
        String bienvenida = userValues.getName() + ", " + Util.getInstance().getEdad(userValues.getDate()) + " años.";
        txtNombre.setText(bienvenida);

        imgPerfil = (ImageView)findViewById(R.id.imgPerfil);
        Picasso.with(this).load(mAuth.getCurrentUser().getPhotoUrl()).placeholder(R.mipmap.ic_launcher).into(imgPerfil);

        btnAgregarHorario = (Button)findViewById(R.id.btnAgregarHorario);

        btnAgregarHorario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                agregarHorario();
            }
        });

        initHorarioDisponible();
        leerHorarioDisponible();
    }

    private void initHorarioDisponible() {
        horarioDisponible.put(LUNES, "NO DISPONIBLE");
        horarioDisponible.put(MARTES, "NO DISPONIBLE");
        horarioDisponible.put(MIERCOLES, "NO DISPONIBLE");
        horarioDisponible.put(JUEVES, "NO DISPONIBLE");
        horarioDisponible.put(VIERNES, "NO DISPONIBLE");
        horarioDisponible.put(SABADO, "NO DISPONIBLE");
        horarioDisponible.put(DOMINGO, "NO DISPONIBLE");
    }

    private void agregarHorario() {

        final Dialog dialog = new Dialog(this);

        dialog.setContentView(R.layout.horario_item_layout);
        dialog.setTitle("Ingrese nuevo horario");

        final Spinner spnDia = (Spinner) dialog.findViewById(R.id.spnDia);
        final Spinner spnDesde = (Spinner) dialog.findViewById(R.id.spnDesde);
        final Spinner spnHasta= (Spinner) dialog.findViewById(R.id.spnHasta);
        final CheckBox chkTodoElDia = (CheckBox) dialog.findViewById(R.id.chkTodoElDia);
        Button btnGuardar = (Button)dialog.findViewById(R.id.btnGuardar);

        loadSpinner(spnDia, getDays());
        loadSpinner(spnDesde, getHours());
        spnDesde.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int value = Integer.parseInt(spnDesde.getSelectedItem().toString());
                loadSpinner(spnHasta, getHours(value+1));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //loadSpinner(spnHasta, getHours());

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                String value = chkTodoElDia.isChecked()?"TODO EL DÍA" : spnDesde.getSelectedItem().toString() + " - " + spnHasta.getSelectedItem().toString();
                horarioDisponible.put(spnDia.getSelectedItem().toString(), value);
                leerHorarioDisponible();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
//This makes the dialog take up the full width
        lp.width = this.getResources().getDisplayMetrics().widthPixels - (int) Util.convertDpToPixel(20f, this);
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog.show();



    }

    private void leerHorarioDisponible(){
        Set set = horarioDisponible.entrySet();
        Iterator iterator = set.iterator();
        txtHorarioDisponible.setText("");
        while(iterator.hasNext()) {
            Map.Entry mentry = (Map.Entry)iterator.next();
            System.out.print("key is: "+ mentry.getKey() + " & Value is: ");
            System.out.println(mentry.getValue());
            txtHorarioDisponible.setText(txtHorarioDisponible.getText().toString() + mentry.getKey() + " : " + mentry.getValue() + "\n");
        }

    }

    private List<String> getHours() {
        return getHours(9);
    }

    private List<String> getHours(int desde) {
        ArrayList<String> values = new ArrayList<>();
        for(int x = desde; x <= 23 ; x++){

            values.add((x<10?"0"+x:x+""));
        }
        return values;
    }

    private void loadSpinner(Spinner spn, List<String> values) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, values);
        spn.setAdapter(adapter);
    }

    private ArrayList<String> getDays(){
        ArrayList<String> values = new ArrayList<>();
        values.add(LUNES);
        values.add(MARTES);
        values.add(MIERCOLES);
        values.add(JUEVES);
        values.add(VIERNES);
        values.add(SABADO);
        values.add(DOMINGO);

        return values;
    }
}
