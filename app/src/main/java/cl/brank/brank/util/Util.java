package cl.brank.brank.util;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;

import org.threeten.bp.LocalDate;
import org.threeten.bp.Period;

/**
 * Created by hardroidlabs on 01-03-17.
 */

public class Util {

    private static Util instance;

    private Util(){

    }

    public static Util getInstance(){
        if(instance == null){
            instance = new Util();
        }
        return instance;
    }

    public int getEdad(int year, int month, int day){

        LocalDate birthdate = LocalDate.of(year, month, day);

        LocalDate now =  LocalDate.now();
        Period period =  Period.between(birthdate, now);
        return period.getYears();

    }

    public int getEdad(LocalDate birthdate){
        LocalDate now =  LocalDate.now();
        Period period =  Period.between(birthdate, now);
        return period.getYears();

    }

    public static float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    public static float convertPixelsToDp(float px, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }


}
