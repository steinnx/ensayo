package cl.brank.brank;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Random;

import cl.brank.brank.models.Contrincante;
import cl.brank.brank.models.UserValues;

public class BusquedaContrincantesActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private ListView lvContrincantes;
    private ArrayList<Contrincante> dataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_busqueda_contrincantes);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Listar Contrincantes
        lvContrincantes = (ListView)findViewById(R.id.lvListarBusquedaDeContrincante);
        ContrincantesAdapter adaptador = new ContrincantesAdapter(this, getDataSource());
        lvContrincantes.setAdapter(adaptador);
        //fin
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private ArrayList<Contrincante> getDataSource(){
        if(dataSource == null) {
            dataSource = new ArrayList<>();

            for (int x = 0; x < 50; x++) {
                //nombre y apellido aleatorio
                String[] nombres = {"Jorge", "Harttin", "Ines", "Isabel","Cristian","Tania","Marta","Esteban"};
                String nombre = nombres[(int) (Math.random() * 8)];
                String[] apellidos = {"Gonzales", "Perez", "Cortes", "Rivas","Avendaño","Fuentes","Sanchez","Sepulveda"};
                String apellido = apellidos[(int) (Math.random() * 8)];
                //Horario aleatorio
                String[] horarios1 = {"Martes: 18:30-21:30", "Lunes: 17:50-20:00", "Jueves: 21:00-24:00", "Sabado: 13:00-22:00","Domingo: 15:00-21:00","Viernes: 18:00-24:00","Jueves: 20:00-24:00","Viernes: 17:00-21:00"};
                String horario1 = horarios1[(int) (Math.random() * 8)];
                String[] horarios2 = {"Martes: 18:30-21:30", "Lunes: 17:50-20:00", "Jueves: 21:00-24:00", "Sabado: 13:00-22:00","Domingo: 15:00-21:00","Martes: 10:00-14:00","Sabado: 20:00-24:00","Viernes: 10:00-21:00"};
                String horario2 = horarios2[(int) (Math.random() * 8)];
                //edad, categoria y ranking aleatorio
                int edad = (int) (Math.random() * 40) + 15;
                int categoria = (int) (Math.random() * 8000) + 1;
                int ranking = (int) (Math.random() * 2000) + 1;
                //genero un contrincante
                Contrincante aux = new Contrincante(nombre+" "+apellido,"https://www.templateplazza.com/templates/nutp/images/man.png",edad,"Categoria: "+categoria,"Ranking: "+ranking,horario1+" / "+horario2);
                dataSource.add(aux);
            }
        }
        return dataSource;
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.busqueda_contrincantes, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_busqueda) {
            // Handle the camera action
        } else if (id == R.id.nav_invitaciones) {

        } else if (id == R.id.nav_encuentros) {

        } else if (id == R.id.nav_calendario) {

        } else if (id == R.id.nav_resultados) {

        } else if (id == R.id.nav_evaluaciones) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
