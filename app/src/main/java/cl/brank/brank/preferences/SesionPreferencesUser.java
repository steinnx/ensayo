package cl.brank.brank.preferences;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import cl.brank.brank.R;
import cl.brank.brank.models.UserValues;

/**
 * Created by hardroidlabs on 01-03-17.
 */

public class SesionPreferencesUser {

    private static SesionPreferencesUser instance;

    private SesionPreferencesUser(){}

    public static SesionPreferencesUser getInstance(){
        if(instance == null){
            instance = new SesionPreferencesUser();
        }
        return instance;
    }

    public void saveUserSesion(Context context, UserValues values){
        SharedPreferences sharedPref = getSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(context.getString(R.string.saved_user_values), values.toString());
        editor.commit();
    }

    public UserValues getUserSesion(Context context){
        return UserValues.getUserValues(
                getSharedPreferences(context).
                        getString(
                                context.getString(R.string.saved_user_values),""));
    }

    private SharedPreferences getSharedPreferences(Context context){
        SharedPreferences sharedPref = context.getSharedPreferences(
                context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        return sharedPref;
    }

}
